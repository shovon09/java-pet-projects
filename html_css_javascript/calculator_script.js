/**
 * Created with IntelliJ IDEA.
 * User: rifatul.islam
 * Date: 4/20/14
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */

Array.prototype.peek = function () {
    return this[this.length - 1];
};

var precedenceMap = {

    '/': 2, // alternate
    '*': 3, // concatenate
    '+': 4, // one or more
    '-': 5, // one or more
    '^': 6 // complement
};

function buttonClicked(buttonValue) {
    switch (buttonValue) {
        case "0":
            displayToMonitor(buttonValue);
            break;
        case "1":
            displayToMonitor(buttonValue);
            break;
        case "2":
            displayToMonitor(buttonValue);
            break;
        case "3":
            displayToMonitor(buttonValue);
            break;

        case "4":
            displayToMonitor(buttonValue);
            break;
        case "5":
            displayToMonitor(buttonValue);
            break;
        case "6":
            displayToMonitor(buttonValue);
            break;
        case "7":
            displayToMonitor(buttonValue);
            break;

        case "8":
            displayToMonitor(buttonValue);
            break;
        case "9":
            displayToMonitor(buttonValue);
            break;

        case "back":
            removeLastFromMonitor();
            break;
        case "clear":
            clearScreen();
            break;
        case "x2":
            displayToMonitor(buttonValue);
            break;
        case "root":
            displayToMonitor(buttonValue);
            break;
        case "(":
            displayToMonitor(buttonValue);
            break;
        case ")":
            displayToMonitor(buttonValue);
            break;
        case "+":
            displayToMonitor(buttonValue);
            break;
        case "-":
            displayToMonitor(buttonValue);
            break;
        case "*":
            displayToMonitor(buttonValue);
            break;
        case "/":
            displayToMonitor(buttonValue);
            break;
        case "%":
            displayToMonitor(buttonValue);
            break;
        case "=":
            infixToPostfixRe("2*(3+4)-5")
           // alert("Sorry No result now  ");
            break;
        default :
            break;
    }
}

function displayToMonitor(value) {
    var input = document.getElementById("monitor").value;
    document.getElementById("monitor").value = input + value;
}
function removeLastFromMonitor() {
    var input = document.getElementById("monitor").value;
    var string = input.replace(/(\s+)?.$/, '');
    document.getElementById("monitor").value = string;

}

function clearScreen() {
    document.getElementById("monitor").value = "";
}

function infixToPostfixRe(reStr, dontPrint) {

    var output = [];
    var stack = [];

    for (var k = 0, length = reStr.length; k < length; k++) {

// current char
        var c = reStr[k];

        if (c == '(')
            stack.push(c);

        else if (c == ')') {
            while (stack.peek() != '(') {
                output.push(stack.pop())
            }
            stack.pop(); // pop '('
        }

// else work with the stack
        else {
            while (stack.length) {
                var peekedChar = stack.peek();

                var peekedCharPrecedence = precedenceOf(peekedChar);
                var currentCharPrecedence = precedenceOf(c);

                if (peekedCharPrecedence >= currentCharPrecedence) {
                    output.push(stack.pop());
                } else {
                    break;
                }
            }
            stack.push(c);
        }

    } // end for loop

    while (stack.length)
        output.push(stack.pop());

    var result = output.join("");

    !dontPrint && console.log(reStr, "=>", result);
    alert("Result  " + result);
    return result;

}


function precedenceOf(c) {
    return precedenceMap[c] || 6;
}
